﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace RandomAccessFile1
{
    public partial class Form1 : Form
    {

        private FileStream raFile = null;

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //Initialize the Random Access File
                raFile = new FileStream("Clients.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);

                if (raFile.Length != 4400)
                {
                    initializeFile();
                }
            }
            catch (IOException ex)
            {
                //Logging
                MessageBox.Show(ex.Message, "Error Creating File");
            }
        }

        //This method initializes the file cabinet (called once and only once!)
        private void initializeFile()
        {
            MessageBox.Show("initializeFile() called");
            AccountRecordRA ra = new AccountRecordRA();

            try
            {
                //Position the file pointer and writes 100 default records
                raFile.Seek(0, SeekOrigin.Begin);
                for (int i = 0; i < 100; i++)
                {
                    ra.Write(raFile);
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message, "Error Initializing File");
            }
        }

        private void cmdInsert_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (IOException ex)
            {
                //Logging
                MessageBox.Show(ex.Message, "Error Inserting Record");
            }
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (IOException ex)
            {
                //Logging
                MessageBox.Show(ex.Message, "Error Updating Record");
            }
        }

        private void cmdDelete_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (IOException ex)
            {
                //Logging
                MessageBox.Show(ex.Message, "Error Deleting Record");
            }
        }

        private void clearText()
        {
            //TODO
        }

        private bool dataGood()
        {
            //TODO
            return true;
        }

        //Will check for primary key violation
        private bool isValidAccount(int acct)
        {
            //TODO
            return true;
        }
        private void setControlState(string state)
        {

        }
    }
}
