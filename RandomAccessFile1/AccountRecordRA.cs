﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RandomAccessFile1
{
    //This Class will perform read/write on parent data
    class AccountRecordRA : AccountRecord
    {
        public AccountRecordRA() 
            : base()
        {
            //Parent no arg constructor called

        }

        public AccountRecordRA(int acct, string fn, string ln, double bal) 
            : base (acct, fn, ln, bal)
        {
            //4 arg parent constructor created
        }

        //This method writes data to the Random Access File
        public void Write(FileStream raFile)
        {
            //Must write record data in field order (pointer moves from left to right)
            BinaryWriter bw = new BinaryWriter(raFile);
            bw.Write(Account);
            formatName(bw, FirstName);
            formatName(bw, LastName);
            bw.Write(Balance);
        }

        //This method reads the data from the Random Access File
        public void Read(FileStream raFile)
        {
            //Need to substitute NULL back to space characters
            BinaryReader br = new BinaryReader(raFile);
            Account = br.ReadInt32();
            FirstName = br.ReadString().Replace('\0', ' ');
            LastName = br.ReadString().Replace('\0', ' ');
            Balance = br.ReadDouble();
        }

        //This method formats the name to the appropriate length
        private void formatName (BinaryWriter bw, string n)
        {
            StringBuilder sb = new StringBuilder(n);
            //From the spec
            sb.Length = 15;
            bw.Write(sb.ToString());
        }
    }
}
